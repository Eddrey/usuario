/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.model.dao.UsuUsuarioDAO;
import root.model.dao.exceptions.NonexistentEntityException;
import root.model.entities.UsuUsuario;

/**
 *
 * @author Lewiss
 */
@WebServlet(name = "Controller", urlPatterns = {"/controller"})
public class Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        //LISTAR USUARIOS
        
        UsuUsuarioDAO dao = new UsuUsuarioDAO();
        List<UsuUsuario> listausuario = dao.findUsuUsuarioEntities();
        request.setAttribute("listausuario", listausuario);
        

        request.getRequestDispatcher("listar.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      
        
        //CREAR USUARIO
        
        String opcion = request.getParameter("opcion");
        
       
         
        if(opcion.equalsIgnoreCase("crear")){
        
            String id = request.getParameter("id");
            String nombres = request.getParameter("nombres");
            String apellidos = request.getParameter("apellidos");
            String correo = request.getParameter("correo");
            String telefono = request.getParameter("telefono");
        
        
            UsuUsuario usuario = new UsuUsuario();
            usuario.setUsuId(id);
            usuario.setUsuNombres(nombres);
            usuario.setUsuApellidos(apellidos);
            usuario.setUsuCorreo(correo);
            usuario.setUsuTelefono(telefono);


            UsuUsuarioDAO dao = new UsuUsuarioDAO();
            try {
                dao.create(usuario);
                Logger.getLogger("log").log(Level.INFO, "valor id usuario: (0)", usuario.getUsuId());
            } catch (Exception ex) {
                Logger.getLogger("log").log(Level.SEVERE, "se presenta un error al crear nuevo usuario: (0)", ex.getMessage());
            }      
        
        
            //request.getRequestDispatcher("salida.jsp").forward(request, response);
            this.doGet(request, response);
         }
        
        // EDITAR USUARIO
        
        //String seleccion = request.getParameter("seleccion");
        //String accion = request.getParameter("accion");
        
        if(opcion.equalsIgnoreCase("editar")){
            String usuarioedit = request.getParameter("usuarioedit");
            //UsuUsuario us = new UsuUsuario();
            UsuUsuarioDAO dao = new UsuUsuarioDAO();
            UsuUsuario us = dao.findUsuUsuario(usuarioedit);
            request.setAttribute("usuario", us);
            request.getRequestDispatcher("editar.jsp").forward(request, response);            
        }
        
        // GRABAR EDICION USUARIO
        
        if(opcion.equalsIgnoreCase("Grabar")){
            String id = request.getParameter("id");
            String nombres = request.getParameter("nombres");
            String apellidos = request.getParameter("apellidos");
            String correo = request.getParameter("correo");
            String telefono = request.getParameter("telefono");
            try{
                UsuUsuarioDAO dao = new UsuUsuarioDAO();
                UsuUsuario us = new UsuUsuario();
                us.setUsuId(id);
                us.setUsuNombres(nombres);
                us.setUsuApellidos(apellidos);
                us.setUsuCorreo(correo);
                us.setUsuTelefono(telefono);
                dao.edit(us);
                //List<UsuUsuario> usuarios = dao.findUsuUsuarioEntities();
                //request.setAttribute("usuarios", usuarios);
                //request.getRequestDispatcher("salida.jsp").forward(request, response);
            }catch (Exception ex){
                 Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
            this.doGet(request, response);
        }
        
        //ELIMINAR USUARIO
        
        if(opcion.equalsIgnoreCase("eliminar")){
            String usuariodelete = request.getParameter("usuariodelete");
            UsuUsuarioDAO dao = new UsuUsuarioDAO();
            //List<UsuUsuario> listausuarios = new ArrayList<>();
            try{
                dao.destroy(usuariodelete);
                //listausuarios = dao.findUsuUsuarioEntities();
            }catch(NonexistentEntityException ex){
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);          
            }
            
            //request.setAttribute("listausuarios", listausuarios);
            //request.getRequestDispatcher("salida.jsp").forward(request, response);
            this.doGet(request, response);
        }    
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
