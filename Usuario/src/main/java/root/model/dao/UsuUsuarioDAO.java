/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.model.dao.exceptions.NonexistentEntityException;
import root.model.dao.exceptions.PreexistingEntityException;
import root.model.entities.UsuUsuario;

/**
 *
 * @author Lewiss
 */
public class UsuUsuarioDAO implements Serializable {

    public UsuUsuarioDAO(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public UsuUsuarioDAO() {
    }
    
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(UsuUsuario usuUsuario) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(usuUsuario);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findUsuUsuario(usuUsuario.getUsuId()) != null) {
                throw new PreexistingEntityException("UsuUsuario " + usuUsuario + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(UsuUsuario usuUsuario) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            usuUsuario = em.merge(usuUsuario);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = usuUsuario.getUsuId();
                if (findUsuUsuario(id) == null) {
                    throw new NonexistentEntityException("The usuUsuario with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UsuUsuario usuUsuario;
            try {
                usuUsuario = em.getReference(UsuUsuario.class, id);
                usuUsuario.getUsuId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The usuUsuario with id " + id + " no longer exists.", enfe);
            }
            em.remove(usuUsuario);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<UsuUsuario> findUsuUsuarioEntities() {
        return findUsuUsuarioEntities(true, -1, -1);
    }

    public List<UsuUsuario> findUsuUsuarioEntities(int maxResults, int firstResult) {
        return findUsuUsuarioEntities(false, maxResults, firstResult);
    }

    private List<UsuUsuario> findUsuUsuarioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(UsuUsuario.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public UsuUsuario findUsuUsuario(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(UsuUsuario.class, id);
        } finally {
            em.close();
        }
    }

    public int getUsuUsuarioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<UsuUsuario> rt = cq.from(UsuUsuario.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
