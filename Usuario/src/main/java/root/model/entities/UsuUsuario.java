/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Lewiss
 */
@Entity
@Table(name = "usu_usuarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsuUsuario.findAll", query = "SELECT u FROM UsuUsuario u"),
    @NamedQuery(name = "UsuUsuario.findByUsuId", query = "SELECT u FROM UsuUsuario u WHERE u.usuId = :usuId"),
    @NamedQuery(name = "UsuUsuario.findByUsuNombres", query = "SELECT u FROM UsuUsuario u WHERE u.usuNombres = :usuNombres"),
    @NamedQuery(name = "UsuUsuario.findByUsuApellidos", query = "SELECT u FROM UsuUsuario u WHERE u.usuApellidos = :usuApellidos"),
    @NamedQuery(name = "UsuUsuario.findByUsuCorreo", query = "SELECT u FROM UsuUsuario u WHERE u.usuCorreo = :usuCorreo"),
    @NamedQuery(name = "UsuUsuario.findByUsuTelefono", query = "SELECT u FROM UsuUsuario u WHERE u.usuTelefono = :usuTelefono")})
public class UsuUsuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "usu_id")
    private String usuId;
    @Size(max = 2147483647)
    @Column(name = "usu_nombres")
    private String usuNombres;
    @Size(max = 2147483647)
    @Column(name = "usu_apellidos")
    private String usuApellidos;
    @Size(max = 2147483647)
    @Column(name = "usu_correo")
    private String usuCorreo;
    @Size(max = 2147483647)
    @Column(name = "usu_telefono")
    private String usuTelefono;

    public UsuUsuario() {
    }

    public UsuUsuario(String usuId) {
        this.usuId = usuId;
    }

    public String getUsuId() {
        return usuId;
    }

    public void setUsuId(String usuId) {
        this.usuId = usuId;
    }

    public String getUsuNombres() {
        return usuNombres;
    }

    public void setUsuNombres(String usuNombres) {
        this.usuNombres = usuNombres;
    }

    public String getUsuApellidos() {
        return usuApellidos;
    }

    public void setUsuApellidos(String usuApellidos) {
        this.usuApellidos = usuApellidos;
    }

    public String getUsuCorreo() {
        return usuCorreo;
    }

    public void setUsuCorreo(String usuCorreo) {
        this.usuCorreo = usuCorreo;
    }

    public String getUsuTelefono() {
        return usuTelefono;
    }

    public void setUsuTelefono(String usuTelefono) {
        this.usuTelefono = usuTelefono;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuId != null ? usuId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuUsuario)) {
            return false;
        }
        UsuUsuario other = (UsuUsuario) object;
        if ((this.usuId == null && other.usuId != null) || (this.usuId != null && !this.usuId.equals(other.usuId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.model.entities.UsuUsuario[ usuId=" + usuId + " ]";
    }
    
}
