<%-- 
    Document   : crear
    Created on : 18-04-2020, 1:25:33
    Author     : Lewiss
--%>

<%@page import="root.model.entities.UsuUsuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    UsuUsuario usuario = (UsuUsuario) request.getAttribute("usuario");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="estilo.css">
        <title>Editar usuarios</title>
    </head>
    <body>
        <div class="form-register">
        <h1>Edición de Usuarios</h1>
        <form action="controller" method="POST">
            
            <input class="controls" type="text" readonly name="id" value="<%=usuario.getUsuId()%>" /><br>            
            <input class="controls" type="text" name="nombres" value="<%=usuario.getUsuNombres()%>" /><br>            
            <input class="controls" type="text" name="apellidos" value="<%=usuario.getUsuApellidos()%>" /><br>            
            <input class="controls" type="text" name="correo" value="<%=usuario.getUsuCorreo()%>" /><br>
            <input class="controls" type="text" name="telefono" value="<%=usuario.getUsuTelefono()%>" /><br>
            
           <!--<button type="submit" name="opcion" value="grabar">Grabar</button>
           <button type="submit" name="opcion" value="salir">Salir</button>-->
           <input class="botoncrear" type="submit"  name="opcion" value="Grabar" />    
        </form>        
        <form action="controller" method="GET">
            <input class="botonlistar" type="submit"  name="opcion" value="Salir" />            
        </form>
    </div>        
    </body>
</html>
