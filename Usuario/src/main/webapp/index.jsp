<%-- 
    Document   : index
    Created on : 15-04-2020, 16:40:20
    Author     : Lewiss
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="estilo.css">
        <title>Crear usuarios</title>
    </head>
    <body>   
        <div class="form-register">
            <h1>Registro de Usuarios</h1>
                <form action="controller" method="POST"> 
                    
                    <input class="controls" type="text" name="id" placeholder="Ingrese su ID" required/><br>
                    <input class="controls" type="text" name="nombres" placeholder="Ingrese sus nombres"  required/><br>                    
                    <input class="controls" type="text" name="apellidos" placeholder="Ingrese sus apellidos"  required/><br>                  
                    <input class="controls" type="text" name="correo" placeholder="Ingrese su email"  required/><br>                  
                    <input class="controls" type="text" name="telefono" placeholder="Ingrese su teléfono"  required/><br>
                    
                    <input class="botoncrear" type="submit" value="Crear Usuario" name="create" /> 
                   <input type="hidden" name="opcion" value="crear"/>
                </form>

                <form name="listado" action="controller" method="GET">
                    <input class="botonlistar" type="submit" value="Listar Usarios" />
                </form>
        </div>          
    </body>
</html>
