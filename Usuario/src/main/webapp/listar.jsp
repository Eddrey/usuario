<%-- 
    Document   : salida
    Created on : 15-04-2020, 17:23:44
    Author     : Lewiss
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="root.model.entities.UsuUsuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    List<UsuUsuario> listausuario = (List<UsuUsuario>) request.getAttribute("listausuario");
    Iterator<UsuUsuario> itListausuario = listausuario.iterator();
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="estilo.css">
        <title>Listar usuarios</title>
    </head>
    <body>
        <div class="table-container">
        <!--<h1>Listado de Usuarios</h1>-->
        <!--<form name="form" action="controller" method="POST">-->
            <table id="tabla">
                <thead>
                    <th>ID</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Email</th>
                    <th>Teléfono</th>
                    <th></th>
                    <th></th>
                </thead>
                <tbody>
                    <%while (itListausuario.hasNext()){
                        UsuUsuario adusu = itListausuario.next();%>
                    <tr>
                        <td><%=adusu.getUsuId()%>
                        <td><%=adusu.getUsuNombres()%></td>
                        <td><%=adusu.getUsuApellidos()%></td>
                        <td><%=adusu.getUsuCorreo()%></td>
                        <td><%=adusu.getUsuTelefono()%></td>
                        <!--<td><input type="radio" name="seleccion" value="<%=adusu.getUsuNombres()%>"></td>
                        <td><button type="submit" name="accion" value="<%=adusu.getUsuId()%>">Editar </button></td>
                        <td><button type="submit" name="accion" value="<%=adusu.getUsuApellidos()%>">Eliminar</button></td>-->
                        <td>    
                        <form name="form" action="controller" method="POST">
                            <button type="submit" name="opcion" value="editar">Editar </button>
                            <input type="hidden" name="usuarioedit" value="<%=adusu.getUsuId()%>" />
                        </form>
                        </td>
                        <td>    
                         <form name="form" action="controller" method="POST">
                            <button type="submit" name="opcion" value="eliminar">Eliminar </button>
                            <input type="hidden" name="usuariodelete" value="<%=adusu.getUsuId()%>" />          
                        </form>
                        </td>
                    </tr>
                    
                    <%}%>
                    
                </tbody>
            </table>
        <!--</form>-->        
    </div>   
    <div class="otrousuario">                
    <a href="index.jsp">Crear otro usuario</a>
    <!--<a href="/">Crear otro usuario</a>--> 
    </div>
    </body>
</html>
